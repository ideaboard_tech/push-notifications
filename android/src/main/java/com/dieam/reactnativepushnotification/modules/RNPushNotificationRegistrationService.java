package com.dieam.reactnativepushnotification.modules;


/**
 * Created by gaudamthiyagarajan on 17/04/18.
 */

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import android.content.SharedPreferences;

import static com.dieam.reactnativepushnotification.modules.RNPushNotification.LOG_TAG;
import static com.dieam.reactnativepushnotification.modules.RNPushNotificationHelper.TOKEN_PREFERENCE_KEY;

public class RNPushNotificationRegistrationService extends FirebaseInstanceIdService {

    private static final String TAG = "RNPushNotification";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(LOG_TAG, "PUSH Refreshed token: " + refreshedToken);
        sendRegistrationToken(refreshedToken);
    }

    private void sendRegistrationToken(String token) {
        Log.v(TAG, "MyFirebaseMsgService: " + token);
        SharedPreferences tokenPersistence = getApplicationContext().getSharedPreferences(TOKEN_PREFERENCE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = tokenPersistence.edit();
        editor.putString(TOKEN_PREFERENCE_KEY, token);
        if (Build.VERSION.SDK_INT < 9) {
            editor.commit();
        } else {
            editor.apply();
        }
        Intent intent = new Intent(this.getPackageName() + ".RNPushNotificationRegisteredToken");
        intent.putExtra("token", token);
        sendBroadcast(intent);
    }
}
